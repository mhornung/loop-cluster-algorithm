/***************************************************
 * A prototype stand alone program to do a simulation...
 * 
 * 
 * With the libraries loop and lattice, loop-cluster monte carlo simulations
 * of varius quantum spin models can be done. In euclidean time as well as 
 * in real time (driven by a measurement process.)
 *
 * The size of the system and various other things can be specisfied as argument to main
 * The Cluster rules are expected to be provided in the specified files.
 *
 * In the File lattice.c the geometry and the input parsing is specified
 * In the File loop.c the loop cluster algorithm is written As a random generator ranlxd is used
 *
 *
 * The following is an example program, wich should 
 * show how to use them.
 *
 ***********************************************/

#include "loop.h"
#include "lattice.h"
#include "observables.h"
#include "io.h"

int main(int argc,char *argv[])
{

		lp_initrandgen(); //initializes the random generator with the system time
		
		/*
		 * Two structs are declared. they will contain all information about the 
		 * geometry and the current state of the spin system.
		 * one represents the real time part, and one the euclidean time part of
		 * the simulated system.
		 * The pointer to these is passed to all functions wich need information.		
		 */
		lattice real_world;
		lattice eucl_world;

		//The struct conf will contain all information about the external parameters of the system. 
		configuration conf;

		//Reads configuration(incl cluster rules) from commandline or file  and save it to the configuration struct
		io_getconf(&conf,argc,argv);
		//Print conf to command line
		io_printconf(&conf);

		//Memory is allocated to store state and geometry of the system.
		//Geometry and cluster rules are initialized in the lattice structs eucl_world and real_world
		if(lat_linitall(&eucl_world, &real_world, &conf)==1) return 1;
	
		//The actual simulation will look something like this: 
		int i;

		//Example observable: clustersize
		
		double etcsize = 0;				
		
		//Thermalize
		for (i = 0; i < 5000; ++i)
		{
			lp_etloop(&eucl_world);
			lp_rtloop(&real_world, &eucl_world);
			lp_flip(&eucl_world);
			lp_flip(&real_world);

		}

		for (i = 0; i <  conf.num_steps; i++)
		{
			lp_etloop(&eucl_world);
			lp_rtloop(&real_world, &eucl_world);
			lp_flip(&eucl_world);
			lp_flip(&real_world);
			
			/****************************
			 * 
			 * Calculate your favourite observable here
			 *
			 ***************************/
			
			etcsize+=eucl_world.loop_length;
		}
		
		etcsize = etcsize / conf.num_steps;

		printf("\net csize : %lf\n", etcsize);
			
		return 0;
}


