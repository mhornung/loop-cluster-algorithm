#ifndef LOOP
#define LOOP

#include "lattice.h"
#include "ranlxs.h"
#include <stdio.h>
#include <time.h>


/*
 * creates a single euclidean time loop according to the cluster rules in world and returns it's length.
 * the starting point is random.
 * all points in the loop are marked with .is_in_loop = 1
 * and their index saved to world.points_in_loop
 * if a point in the loop is in the first time slice it's index
 * is stored to the array world.real_time_intersections
 * the length of this array is updated (world.current_num_rti)
 * the plaquette state is saved in th world.plaquette array
 */
int lp_etloop(lattice * world);

/*
 * Creates a loop in real_world for each point in the array eucl_world.real_time_intersections as starting point.
 * The point indices for all loops are consecutively stored in real_world.points_in_loop.
 * Returns the number total number of points in all loops created
 * works like lp_etloop. But:
 * as starting points, the points in the array eucl_world.real_time_intersections are used.
 * each loop goes only foreward in time.
 * each loop ends if it reaches the last timeslice.
 * the array eucl_world.real_time_intersections and eucl_world.current_num_rti are reset to 0. 
 */
int lp_rtloop(lattice * real_world, lattice * eucl_world);

/* 
 * for all points in world.points_in_loop:
 * 	all associated plaquette breakups (world.plaquette) are set to 0
 * 	world.point[...].is_in_loop is set to 0
 * 	world.point[...].spin is flipped
 * 
 * Note: the array world.point_in_loop is NOT reset! It is therefor important to keep track of
 * the length of the loop reset (done in world.loop_length)
 */
int lp_flip(lattice * world);

//Initializes the random generator used (ranluxd) 
void lp_initrandgen();
#endif
