#ifndef IO
#define IO
#include "lattice.h"
#include <stdio.h>
#include <string.h>

int io_getconf(configuration * conf, int argc, char *argv[]);
int io_getconf_file(configuration * conf, char * cfname);
int io_printconf(configuration * conf);

int io_append_comment(char * fname, char * comment);
int io_append_to_file(char * fname,double * data, int numvalues, int vpline);
int io_print_header(char * fname, char * header);


#endif
