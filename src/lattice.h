#ifndef LATTICE_UNIVERSAL	
#define LATTICE_UNIVERSAL
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <string.h>

//the lattice point contains all information about geometry 
typedef struct lattice_point_s lattice_point;
struct lattice_point_s
{	
//The timelike part of the  lattice including plaquetting
// plaquette future is the number of the plaquette in positive time direction
// plaquette_future_neighbour is the equaltime neighbour in the future plaquette
	int neighbour_future;
	int neighbour_past;

	int plaquette_future;
	int plaquette_future_neighbour;
	int plaquette_past;
	int plaquette_past_neighbour;	

	int is_in_loop;
	
	int spin;
};


/*
 * The lattice will contain all information about the geometry and state of the 
 * lattice, and the cluster rules.
 * There is an array of lattice_point 's (point) in wich the spins and the plaquette geometry is stored
 * In the array point_in_loop, the index' of all points in the loop at a given time are stored.
 * the plaquette array contains the state of each plaquette  (0 not set, 1 A, 2 B, 3 C ) the index refers to the point in the past left/upper corner of the plaquette
 * in the real_time_intersections array all points in the loop in the first time slice are stored. current_num_rti is the length of this array
 */

typedef struct lattice_s lattice;
struct lattice_s
{
	int size_1;
	int size_2;
	int size_time;
	int num_points;
	
	double crules[16][3];

	int loop_length;

	int * point_in_loop;
	int * plaquette;
	lattice_point * point;

	//for the real time
	int * real_time_intersections;
	int current_num_rti;
};

//The configuration will contain all input parameters to the simulation.
typedef struct configuration_s configuration;
struct configuration_s
{	
	char comment[400];
	
	//enables the program to calculate different modes of susc without recompiling
	double suscmode_p1;
	double suscmode_p2;
		
	int size_1;
	int size_2;
	int size_eucl_time;
	int size_real_time;
	int size_time;

	char crfile_et[100];
	char crfile_rt[100];
	char result_file[100];

	double etcrules[16][3];
	double rtcrules[16][3];

	long num_steps;
	
	double eps;
	double j;
	double jprime;

	int staggered_ic;
	
};


/* Prints the lattice in world to stdout
 * each point is numbered with its position
 * it's spin is printed right after that
 * in the lines between the points the breakups set, are
 * represented by H (A) , X (C), = (B)
 * All Points in the loop are printed in purple
 */
void lat_prlattice(lattice * world);

/* the lattices eucl_world and real_world are initialized according to conf
 * Memory is allocated for the points and the geometry is specified
 * clusterrules are set to the specified in conf
 * the function returns 0
 *
 * To do only euclidean time simulation a real (dummy) world must be initialized nevertheless
 * it can have 0 time extent.
 */
int lat_linitall(lattice * eucl_world, lattice * real_world, configuration * conf);

/* Hashfunction for the lattice points.
 * it returns the array index for coordinate x_1, x_2 and x_t
 * the Periodic boundry conditions are implemented in this function, it is used
 * to create the lattice geometry in lat_linitall(...)!
 * The functions works for arbitrary (even negative) coordinates.
 */
int lat_ctopos(lattice * world, int x_1, int x_2, int x_t);

/*The inverse function to lat_ctopos(...) the result is written to the
 * array coordinates. This MUST be an array of at least length 3 else
 * segfault.
 */
int lat_pos_to_coord_3d(lattice * world, int pos, int * coordinates);

#endif

