#include "loop.h"

static int choose_breakup(lattice * world, int position, int direction);
static int set_breakup(lattice * world, int position, int direction, int breakup);
static int rlp_create_single_rtime_loop(lattice * world, int starting_position, int points_already_in_loop);
//choses and returns the breakup in the active plaquette
static int choose_breakup(lattice * world, int position, int direction)
{

	float random_number[1];

	//decide which is the active plaquette.
	int pos_plaq_neighbour_space;
	int pos_plaq_neighbour_time;
	int pos_plaq_neighbour_diag;

	int plaq_state;
		
	if( direction == 1){
		pos_plaq_neighbour_space = world->point[position].plaquette_future_neighbour;
		pos_plaq_neighbour_time = world->point[position].neighbour_future;
		pos_plaq_neighbour_diag = world->point[pos_plaq_neighbour_space].neighbour_future;
		
	} else {
		
		pos_plaq_neighbour_space = world->point[position].plaquette_past_neighbour;
		pos_plaq_neighbour_time = world->point[position].neighbour_past;
		pos_plaq_neighbour_diag = world->point[pos_plaq_neighbour_space].neighbour_past;
	} 

	//Take their spins
	int spin_position = world->point[position].spin;
	int spin_plaq_neighbour_space = world->point[pos_plaq_neighbour_space].spin;
	int spin_plaq_neighbour_time = world->point[pos_plaq_neighbour_time].spin;
	int spin_plaq_neighbour_diag = world->point[pos_plaq_neighbour_diag].spin;

	//Plaquette state hash to accsess the cluster rules for the current plaquette state
	plaq_state = spin_position + 2 * spin_plaq_neighbour_space + 4 * spin_plaq_neighbour_time + 8 * spin_plaq_neighbour_diag; 

	ranlxs(random_number, 1);
	int i=0;
	float c_prob = (float) world->crules[plaq_state][0];

	//Chose randomly according to the cluster rules wich are stored in world->cluster_rules...
	//See crules_init in lattice_universal.c for documentation.		
	while(c_prob <= random_number[0])  {
			i++;
			if (i==3) {
					printf("ups!");
#ifdef DEBUG_ON
				printf("\nsomething wrong: invalid breakup chosen! check cluster rules for phash %d!\n",plaq_state);
				printf("position: %d\n Spin: %d \n",position, spin_position);
				printf("pnspace: %d\n Spin: %d \n",pos_plaq_neighbour_space, spin_plaq_neighbour_space);
				printf("pntime: %d\n spin: %d\n",pos_plaq_neighbour_time, spin_plaq_neighbour_time);
				printf("pndiag: %d\n Spin: %d\n ",pos_plaq_neighbour_diag, spin_plaq_neighbour_diag);
				fflush(stdout);
				lat_prlattice(world);
				getchar();
				return 0;
#endif
				//To eliminiate rounding errors from loading the cluster rules.
//				return 3; 
			}
			c_prob+= world->crules[plaq_state][i];
	}
	//Breakups go from 1 to 3 
	//while the breakup index in c rules starts at 0
	return i+1;
}
static int set_breakup(lattice * world, int position, int direction, int breakup)
{
	if( direction == 1 ){
 		world->plaquette[world->point[position].plaquette_future] = breakup;
	}else{
 		world->plaquette[world->point[position].plaquette_past] = breakup;
	}
	return 0;
}
//Creates a loop which is appended to world.point_in_loop[] (the number of points already in the loop is therefor needed)) and returns the length of the loop
//This Function Works just as the function to create a euclidean time loop. Only instead of a
//random starting Point, the starting point is given, and it stops not if the loop closes, but if it reaches the 
//final time slice. This is realized by adding exactly world.size_time points to the loop since the real time loop goes only foreward in time (no B breakup).
//The direction switching thing is therefor also not needed.
static int rlp_create_single_rtime_loop(lattice * world, int starting_position, int points_already_in_loop)
{
	int position = starting_position;
	int direction = 1; //direction is always one since the real time loop goes always foreward
	int n = 1;
	int current_breakup =0;
	world->point_in_loop[points_already_in_loop+n-1]=position;
	world->point[position].is_in_loop = 1;	
	
	//repeat until the loop reaches the last time slice
	do{
		n++;
	

			//if the breakup of the future plaquette is not set,  set it.
			if( world->plaquette[world->point[position].plaquette_future] == 0){
			
				current_breakup = choose_breakup(world,position, direction);

				#ifdef DEBUG_ON
				printf("set breakup to %d",current_breakup);
				fflush(stdout);
				#endif			

				set_breakup(world, position, direction, current_breakup);

				#ifdef DEBUG_ON			
				printf("done\n");
				fflush(stdout);
				#endif			

			} else {
				current_breakup = world->plaquette[world->point[position].plaquette_future];  
			}
			//follow the breakup
			switch(current_breakup){
				case 1: 
					position = world->point[position].neighbour_future;
					break;
				case 3:	
					position = world->point[world->point[position].plaquette_future_neighbour].neighbour_future;
					break;
				}
		world->point_in_loop[points_already_in_loop+n-1]=position;
		//only required by the function print_lattice_breakups from lattice_universal.c
		world->point[position].is_in_loop = 1;	

#ifdef DEBUG_ON
		printf("real one! n :%d", n);
		lat_prlattice(world);
		getchar();
#endif
	
	}while(n < world->size_time);  
	
	return n;
}

//External functions (documented in the header file)
int lp_etloop(lattice * world)
{
	float random_number[1];
	//select random starting point
	ranlxs(random_number,1);//Generates a random number between 0 and 1 wich is stored in random number
	int start = (int) (random_number[0] * world->num_points);
	int position = start;
	int direction = 1; //1 if the next plaquette is in future 0 if in past	
	int n = 0;
	int current_breakup =0;
	world->current_num_rti = 0;
	//repeat until the loop closes
	do{
//If compiled for debuging print lattice at every step	
#ifdef DEBUG_ON
		lat_prlattice(world);
		getchar();
#endif
		world->point_in_loop[n]=position;
		//only required by the function lat_prlattice from lattice_universal.c
		//and to determine wich boundry points are in the loop (for real time loop starting points) 
		world->point[position].is_in_loop = 1;	
		n++;
		
		//store realtime intersection points (as position of the starting point in the 
		// real time lattice) in the array real_time_intersections belonging to the euclidean
		// time lattice
		if (position < world->size_1 * world->size_2)
		{
			world->real_time_intersections[world->current_num_rti]=position;
			world->current_num_rti ++;
		}


		if(direction == 1){

			//if the breakup of the future plaquette is not set,  set it.
			if( world->plaquette[world->point[position].plaquette_future] == 0){
				current_breakup = choose_breakup(world,position, direction);
				set_breakup(world, position, direction, current_breakup);
			} else {
				current_breakup = world->plaquette[world->point[position].plaquette_future];  
			}
			//follow the breakup
			switch(current_breakup){
				case 1: 
					position = world->point[position].neighbour_future;
					break;
				case 2:
					position = world->point[position].plaquette_future_neighbour;
					direction = 0; // Switch direction on b breakups
					break;
				case 3:	
					position = world->point[world->point[position].plaquette_future_neighbour].neighbour_future;
					break;
				}	
		} else {	
				if( world->plaquette[world->point[position].plaquette_past] == 0){
				current_breakup = choose_breakup(world,position, direction);
				set_breakup(world, position, direction, current_breakup);
			} else {
				current_breakup = world->plaquette[world->point[position].plaquette_past];  
			}
			//follow the breakup
			switch(current_breakup){
				case 1: 
					position = world->point[position].neighbour_past;
					break;
				case 2:
					position = world->point[position].plaquette_past_neighbour;
					direction = 1; // Switch loop direction on b breakups
					break;
				case 3:	
					position = world->point[world->point[position].plaquette_past_neighbour].neighbour_past;
					break;
				}	
		}
		
	}while(start != position);  
	
	world->loop_length = n;
	
	return n;

}
	
int lp_flip(lattice * world)
{
	int i = 0;
	int pos, plaq_pos_1, plaq_pos_2;
	int loop_length = world->loop_length;
	for (i = 0; i < loop_length; i++) {
		pos = world->point_in_loop[i];
		plaq_pos_1 = world->point[pos].plaquette_future;
		plaq_pos_2 = world->point[pos].plaquette_past;
		
		world->plaquette[plaq_pos_1] = 0;
		world->plaquette[plaq_pos_2] = 0;
		world->point[pos].is_in_loop = 0;
		//flip the spins
		if(world->point[pos].spin == 1) world->point[pos].spin = 0;	 
		else 							world->point[pos].spin = 1;	 

	}
	return 0;
}

int lp_rtloop(lattice * real_world, lattice * eucl_world)
{
	int i, position;
	int points_in_loop = 0;
	for (i = 0; i < eucl_world->current_num_rti; i++)
		{
				position = eucl_world->real_time_intersections[i];
				points_in_loop += rlp_create_single_rtime_loop(real_world, position, points_in_loop);
				eucl_world->real_time_intersections[i] = 0;	
	}
	eucl_world->current_num_rti = 0;
	real_world->loop_length = points_in_loop;
	return points_in_loop;
}

void lp_initrandgen()
{
	rlxs_init(1,time(NULL));
	return;
}
