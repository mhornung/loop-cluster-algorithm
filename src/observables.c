#include "observables.h"

double susceptibility_of_state(lattice * world)
{	
	int x,y,t;
	double total_mag=0;
	double mag =0;
	double spin;
	
	for (t = 0; t < world->size_time; ++t)
	{
		mag =0;
			
		for (x = 0; x < world->size_1; x++) {
			for (y = 0; y < world->size_2; y++) {
				spin = world->point[lat_ctopos(world,x,y,t)].spin == 0 ? -1 : 1;
				mag += spin;
			}
	
		}
		total_mag+=mag * mag;
	}
	return total_mag / world->size_time;


}

double obs_one(lattice * world)
{
	return 1.;
}

double stag_susc_of_state(lattice * world)
{
	int x,y,t;
	double total_mag=0;
	double mag =0;
	double spin;
	
	for (t = 0; t < world->size_time; ++t)
	{
		mag =0;
			
		for (x = 0; x < world->size_1; x++) {
			for (y = 0; y < world->size_2; y++) {
					spin = world->point[lat_ctopos(world,x,y,t)].spin == 0 ? -1 : 1;
					if ((x+y)%2 == 0) {
						mag += spin;
					}else{
						mag -= spin;
					}
	
				}
		}
		total_mag+=mag * mag;
	}
	return total_mag / world->size_time;


}
double loop_magnetization(lattice * world, int loop_length)
{
	int pos, i;
	double mag = 0;
	for (i = 0; i < loop_length; i++) {
		pos = world->point_in_loop[i];
		mag += world->point[pos].spin == 0 ? -1 : 1;
	}
	
	return mag;
}
double rt_stag_magnetization(lattice * world, int time)
{
	int x,y;
	double stag_mag=0;
	double spin;
	for (x = 0; x < world->size_1; x++) {
		for (y = 0; y < world->size_2; y++) {
			spin = world->point[lat_ctopos(world,x,y,time)].spin == 0 ? -0.5 : 0.5;
			if ((x+y)%2 == 0) {
				stag_mag += spin;
			}else{
				stag_mag -= spin;
			}
		}
	}
	return stag_mag * stag_mag;
}
double rt_magnetization(lattice * world, int time)
{
	int x,y;
	double mag=0;
	double spin;
	for (x = 0; x < world->size_1; x++) {
		for (y = 0; y < world->size_2; y++) {
			spin = world->point[lat_ctopos(world,x,y,time)].spin == 0 ? -1 : 1;
						mag += spin;
			}
	}
	return mag;
}

double t_total_magnetization(lattice * world)
{	
	int i;
	double total_mag=0;
	int mag =0;
	int spin;
	for (i = 0; i < world->num_points; ++i)
	{
			spin = world->point[i].spin == 0 ? -1 : 1;
			mag += spin;	
	}	
	total_mag = ((double) mag / (double) world->size_time)*((double)mag / (double)world->size_time);
	return  total_mag;
}

double t_total_stag_magnetization(lattice * world)
{	
	int x,y,t;
	double total_mag=0;
	double mag =0;
	double spin;
	
	for (t = 0; t < world->size_time; ++t)
	{
		mag =0;
			
		for (x = 0; x < world->size_1; x++) {
			for (y = 0; y < world->size_2; y++) {
					spin = world->point[lat_ctopos(world,x,y,t)].spin == 0 ? -1 : 1;
					if ((x+y)%2 == 0) {
						mag += spin;
					}else{
						mag -= spin;
					}
	
				}
		}
		total_mag+=mag * mag;
	}
	return total_mag;
}

double obs_suscmode(lattice * world, int time, double p1, double p2)
{
	int x,y;
	double mag_real = 0;
	double mag_im = 0;
	double spin;
	for (x = 0; x < world->size_1; ++x)
	{
			for (y = 0; y < world->size_2; ++y)
			{	
					spin = world->point[lat_ctopos(world,x,y,time)].spin ==0 ? -0.5 :0.5;
					mag_real += cos(p1 * x + p2 * y)*spin; 
					mag_im += sin(p1 * x + p2 * y)*spin; 
			}
			
	}	
	return mag_real * mag_real + mag_im * mag_im;
}
