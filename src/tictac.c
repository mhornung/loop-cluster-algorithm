#include <time.h>
float tic_time;
float tictac_time;
int tic()
{
	tic_time = (float)clock();
	return 0;
}

int tac()
{
	float tac_time;
	tac_time = (float) clock();
	tictac_time = (tac_time - tic_time) / CLOCKS_PER_SEC;
	return 0;
}

float tictac()
{
	return tictac_time;
}
