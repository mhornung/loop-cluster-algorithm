#ifndef OBSERVABLES
#define OBSERVABLES

#include "lattice.h"
#include <math.h>

double susceptibility_of_state(lattice * world);
double obs_one(lattice * world);
double stag_susc_of_state(lattice * world);
double loop_magnetization(lattice * world, int loop_length);
double rt_stag_magnetization(lattice * world, int time);
double rt_magnetization(lattice * world, int time);
double t_total_magnetization(lattice * world);
double t_total_stag_magnetization(lattice * world);
double obs_suscmode(lattice * world, int time, double p1,  double p2);
#endif
