#include "io.h"
static void io_prusage();
static int  io_crinit(double clusterrules[16][3], const char * fname);

static void io_prusage()
{
	printf("\n\
	Valid Arguments \n  \
		-size_1   \n\
		-size_2 \n\
		-size_eucl_time	\n\
		-size_real_time	\n\
		-eps 		\n\
		-numsteps 	\n\
		-stag_ic \n\
		-crfile_et \n\
		-crfile_rt \n\
		-result_file \n\
		-? 			\n\
	If no argument is given, default values are used.	\n\n");
	return;
}


int io_getconf(configuration * conf, int argc, char *argv[])
		{
			int i;
			int n=0;
			// set default values 
			conf->suscmode_p1 = 0;
			conf->suscmode_p2 = 0;
			conf->size_1 = 2;
			conf->size_2 = 2;
			conf->size_time = 10;
			conf->size_real_time = 4;
			conf->size_eucl_time = 4;
			conf->num_steps = 1000;
			conf->staggered_ic = 0;
			strcpy(conf->crfile_et,"etcrules.txt");
			strcpy(conf->crfile_rt,"rtcrules.txt");
			strcpy(conf->result_file,"results.dat");
			// if an optional argument is given use it
			for (i = 1; i < argc; i++) {
				//load configuration from file if the -file option is set, 
				//this  is overriten by all following set options.
				if(strcmp(argv[i], "-suscmode_p1"	)==0){	sscanf(argv[i+1], "%lf",  &conf->suscmode_p1); 	n++;}
				if(strcmp(argv[i], "-suscmode_p2"	)==0){	sscanf(argv[i+1], "%lf",  &conf->suscmode_p2); 	n++;}
				if(strcmp(argv[i], "-size_1"	)==0){	sscanf(argv[i+1], "%d",  &conf->size_1); 	n++;}
				if(strcmp(argv[i], "-size_2"	)==0){	sscanf(argv[i+1], "%d",  &conf->size_2); 	n++;}
				if(strcmp(argv[i], "-size_eucl_time")==0){	sscanf(argv[i+1], "%d",  &conf->size_eucl_time); n++;}
				if(strcmp(argv[i], "-size_real_time")==0){	sscanf(argv[i+1], "%d",  &conf->size_real_time); n++;}
				if(strcmp(argv[i], "-numsteps"	)==0){	sscanf(argv[i+1], "%ld", &conf->num_steps); n++;}
				if(strcmp(argv[i], "-stag_ic"	)==0){	sscanf(argv[i+1], "%d",  &conf->staggered_ic); 	n++;}
				if(strcmp(argv[i], "-crfile_et"	)==0){	sscanf(argv[i+1], "%s",  conf->crfile_et); 	n++;}
				if(strcmp(argv[i], "-crfile_rt"	)==0){	sscanf(argv[i+1], "%s",  conf->crfile_rt); 	n++;}
				if(strcmp(argv[i], "-result_file"	)==0){	sscanf(argv[i+1], "%s",  conf->result_file); 	n++;}
				if(strcmp(argv[i], "-comment"	)==0){	sscanf(argv[i+1], "%s",  conf->comment); 	n++;}
				if(strcmp(argv[i], "-file"	)==0){	io_getconf_file(conf,argv[i+1]); 	n++;}
				if(strcmp(argv[i], "-?"			)==0){	io_prusage(); return 1;}
			}
			if (2*n != argc -1) {
				printf("\nERROR: Invalid Argument\n");
				io_prusage();
				return 1;
			}
			
			
			//check for consistency:
			if (conf->size_eucl_time % 4 != 0)
			{
				printf("Warning: size_eucl_time mod 4 != 0\n");
			}
			if (conf->size_real_time % 4 != 0)
			{
				printf("Warning: size_real_time mod 4 != 0\n");
			}
			io_crinit(conf->etcrules,conf->crfile_et);
			
			io_crinit(conf->rtcrules,conf->crfile_rt);
			return 0;
		}

//The cluster rules in file fname are expected to be given in the following format:
//
//plaquette_hash,prob_A,prob_B,prob_C
//
//plaquette_hash:
//c d		
//a b ->  a + 2*b + 4*c + 8*d   
//Where spin up is 1 and spin down is 0
//
//Note:
//There are no consistency checks!!!
//
//The cluster rules are then stored in the clusterrules array (normaly conf.crules)
//in the form clusterrules[plaquette_hash][breakup] = probability


static int io_crinit(double clusterrules[16][3], const char * fname)
{		
	
	FILE * fcrules;
	
	fcrules = fopen(fname,"r");
	if (fcrules == NULL)
	{
		printf("error opening the cluster_rules file (%s)",fname);
		return 1;
	}

	while( !feof(fcrules) )
	{	
		//read file line by line, and extract
		char line[100];
		fgets(line, sizeof(line), fcrules);
		
		//ignoring lines wich start with #
		if (strncmp(line,"#",1)!=0)
		{
			double c_id;
			double c_prob[3];
			sscanf(line, "%lf,%lf,%lf,%lf", &c_id, &c_prob[0],&c_prob[1], &c_prob[2]);
			int i;
			for (i = 0; i < 3; ++i)
			{
				clusterrules[(int)c_id][i] = c_prob[i];
				
			}
			 
		}
	}
	return 0;
}


int io_getconf_file(configuration * conf, char * cfname)
{
	FILE * cfile = fopen(cfname, "r");
	if (cfile == NULL)
	{
			printf("error opening configuration file (%s)",cfname);
			return 1;
	}
	char line[500];
	char parname[50];
	char parvalue[450];
	while(!feof(cfile)) 
	{

		fgets(line, sizeof(line),cfile);
		if(sscanf(line, "%s %[^\n]",parname, parvalue)!=2){ 
			printf("can't read line: read parname: '%s' parvalue: '%s'",parname,parvalue);
		}
		

		if(strcmp(parname, "size_1"	)==0)			{ sscanf(parvalue, "%d",  &conf->size_1); }	
		if(strcmp(parname, "size_2"	)==0)			{ sscanf(parvalue, "%d",  &conf->size_2); }	
		if(strcmp(parname, "size_eucl_time")==0)	{ sscanf(parvalue, "%d",  &conf->size_eucl_time); }
		if(strcmp(parname, "size_real_time")==0)	{ sscanf(parvalue, "%d",  &conf->size_real_time); }
		if(strcmp(parname, "numsteps"	)==0)		{ sscanf(parvalue, "%ld", &conf->num_steps); }
		if(strcmp(parname, "stag_ic"	)==0)		{ sscanf(parvalue, "%d",  &conf->staggered_ic); 	}
		if(strcmp(parname, "crfile_et"	)==0)		{ sscanf(parvalue, "%s",  conf->crfile_et); 	}
		if(strcmp(parname, "crfile_rt"	)==0)		{ sscanf(parvalue, "%s",  conf->crfile_rt); 	}
		if(strcmp(parname, "suscmode_p1"	)==0)	{ sscanf(parvalue, "%lf",  &conf->suscmode_p1); 	}
		if(strcmp(parname, "suscmode_p2"	)==0)	{ sscanf(parvalue, "%lf",  &conf->suscmode_p2); 	}
		if(strcmp(parname, "result_file"	)==0)		{ sscanf(parvalue, "%s",  conf->result_file); 	}
		if(strcmp(parname, "comment"	)==0)		{ sscanf(parvalue, "%[^\n]",  conf->comment); 	}
	
	}
	return 0;
}

int io_printconf(configuration * conf)
{
	printf("Configuration: \n\
		-size_1 \t%d   \n\
		-size_2 \t%d \n\
		-size_eucl_time	\t%d \n\
		-size_real_time	\t%d \n\
		-numsteps 	\t%ld \n\
		-stag_ic \t%d \n\
		-crfile_et \t%s\n\
		-crfile_rt \t%s\n\
		-comment \t%s\n ",conf->size_1, conf->size_2, conf->size_eucl_time, conf->size_real_time, conf->num_steps, conf->staggered_ic, conf->crfile_et,conf->crfile_rt, conf->comment);
		return 0;
}


int io_append_comment(char * fname, char * comment )
{
	FILE * fresults;
	fresults = fopen(fname,"a");
	if (fresults == NULL)
	{	
		printf("Error Opening th file %s\n", fname);
		return 1;	
	}else{
		fprintf(fresults,"%s\n",comment);

	}
	fclose(fresults);
	return 0;
}

int io_append_to_file(char * fname, double * data, int numvalues, int vpline)
{

		FILE * fresults;
		fresults = fopen(fname, "a+");

		if (fresults == NULL)
		{
			printf("Error Opening the file %s\n", fname);		
			return 1;
		}else{
			int i;
			for (i = 0; i < numvalues; i++)
			{

				fprintf(fresults, "%lf\t",data[i]);
				if (i%vpline==vpline-1)
				{
					fprintf(fresults, "\n");
				}
			}
			fprintf(fresults,"\n");

		}
		fclose(fresults);
	return 0;
}

int io_print_header(char * fname, char * header)
{
	FILE * fresults;
	fresults = fopen(fname, "a+");

	if (fresults == NULL)
	{
		printf("Error Opening the file %s\n", fname);		
		return 1;
	}else{
		char buf;
		fscanf(fresults,"%c", &buf);
		//Check wheter the header is already there
		//if not create it.
		//Start the header with a special caracter! (#)
		if (buf != header[0]){
			fprintf(fresults, "%s\n",header);
		} 
		fclose(fresults);
		return 0;
	}	
}
