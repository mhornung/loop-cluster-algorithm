#include <stdarg.h>
#include "lattice.h"
static int lat_linit();
static void lat_prlatticeslice(lattice * world, int slice);
static int lat_enforce_cr_consistency(double crules[16][3]);

int lat_pos_to_coord_3d(lattice * world, int pos, int * coordinates)
{
	//x_t
	coordinates[2] = pos / (world->size_1*world->size_2);
	//x_2
	coordinates[1] = (pos % (world->size_1*world->size_2)) / world->size_1;
	//x_1
	coordinates[0] = pos - coordinates[2] * (world ->size_1*world->size_2) - coordinates[1] * world ->size_1;
	return 0;
}

//periodic boundry conditions are implemented here!
int lat_ctopos(lattice * world, int x1, int x2, int xt)
{
	int x1periodic = (x1 % world->size_1) < 0 ? (x1 % world->size_1) + world->size_1 : x1 % world->size_1;
	int x2periodic = (x2 % world->size_2) < 0 ? (x2 % world->size_2) + world->size_2 : x2 % world->size_2;
	int xtperiodic = (xt % world->size_time) < 0 ? (xt % world->size_time) + world->size_time : xt % world->size_time;
	return  x1periodic + x2periodic * world->size_1 + xtperiodic * world->size_1 * world->size_2;
}



static int lat_linit(lattice * world, configuration * conf)
{
		
	int size_1 = conf->size_1;
	int size_2 = conf->size_2;
	int size_time = conf->size_time;

	//Check wether the number of rows and columns is even, if not 
	//return error.	
	if (size_1 % 2 != 0 || size_2 % 2 != 0 || size_time %2 != 0){
		printf("Warning: number of rows or columns not even"); 
	//	return 1;
	}

	world->size_1 = size_1;
	world->size_2 = size_2;
	world->size_time = size_time;	
	
	world->num_points = size_1 * size_2 * size_time;

	
	//Allocate the memory for the lattice points and plaquettes and loops
	world->point = malloc(sizeof(lattice_point) * world->num_points);
	world->plaquette = calloc(world->num_points , sizeof(int));
	world->point_in_loop = calloc(world->num_points, sizeof(int));

	int i;
	int current_pos;	

	int x_1,x_2,x_t;
	int coord[3];
	//Spins are initialized to 0;
	for (i = 0; i < world->num_points; i++) {
		world->point[i].spin = 0;

		//set staggered initialconditions (chess board) if the option is set
		if (conf->staggered_ic == 1)
		{
			lat_pos_to_coord_3d(world,i,coord);
			if ((coord[0]+coord[1]+coord[2]) % 2 == 1)
			{
				world->point[i].spin = 1;	
			}	
		
		}
	}
	// initializing plaquette geometry
	for (x_1=0; x_1<world->size_1; x_1 ++){
		for(x_2=0; x_2<world->size_2;x_2++){
			for (x_t = 0; x_t < world->size_time; x_t++) {
				current_pos = lat_ctopos(world, x_1, x_2, x_t);
				world->point[current_pos].neighbour_future = lat_ctopos(world, x_1, x_2, x_t + 1);
				world->point[current_pos].neighbour_past   = lat_ctopos(world, x_1, x_2, x_t - 1);
				//Assining plaquette neighbours and plaquettes
				switch(x_t % 4){
					case 0: 
						if( x_1 % 2 == 0){
							world->point[current_pos].plaquette_future_neighbour=lat_ctopos(world,x_1+1,x_2,x_t);
							world->point[current_pos].plaquette_future = lat_ctopos(world, x_1 , x_2 , x_t);
						}else{
							world->point[current_pos].plaquette_future_neighbour=lat_ctopos(world, x_1 - 1, x_2, x_t);
							world->point[current_pos].plaquette_future = 		 lat_ctopos(world, x_1 - 1, x_2 , x_t);
						} 	
						if(x_2 % 2 == 0){	
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1, x_2 -1,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 , x_2 -1 , x_t -1);
						} else {
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1, x_2 + 1,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 , x_2 , x_t - 1);
						} break;
					case 1:
						if( x_2 % 2 == 0){
							world->point[current_pos].plaquette_future_neighbour=lat_ctopos(world, x_1, x_2 + 1, x_t);
							world->point[current_pos].plaquette_future 		= lat_ctopos(world, x_1, x_2 , x_t);
						}else{
							world->point[current_pos].plaquette_future_neighbour = lat_ctopos(world, x_1, x_2 - 1, x_t);
							world->point[current_pos].plaquette_future 			 = lat_ctopos(world, x_1, x_2 - 1, x_t);
						}

						if(x_1 % 2 == 0){	
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1 + 1, x_2 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 , x_2 , x_t -1);
						} else {
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1 - 1, x_2 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 -1 , x_2 , x_t - 1);
						} break;
					case 2:
						if( x_1 % 2 == 0){
							world->point[current_pos].plaquette_future_neighbour = lat_ctopos(world, x_1 - 1, x_2 , x_t);
							world->point[current_pos].plaquette_future 			 = lat_ctopos(world, x_1 - 1, x_2 , x_t);
	
						}else{
							world->point[current_pos].plaquette_future_neighbour = lat_ctopos(world, x_1 + 1, x_2 , x_t);
							world->point[current_pos].plaquette_future 			 = lat_ctopos(world, x_1, x_2 , x_t);
						} 
						if(x_2 % 2 == 0){	
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1, x_2 + 1 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 , x_2 , x_t -1);
						} else {
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1, x_2 - 1 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 , x_2 - 1 , x_t - 1);
						} break;
					case 3:
						if( x_2 % 2 == 0){
							world->point[current_pos].plaquette_future_neighbour = lat_ctopos(world, x_1, x_2 - 1, x_t);
							world->point[current_pos].plaquette_future 			 = lat_ctopos(world, x_1, x_2 - 1, x_t);

						}else{
							world->point[current_pos].plaquette_future_neighbour=lat_ctopos(world, x_1, x_2 + 1, x_t);
							world->point[current_pos].plaquette_future 		=	 lat_ctopos(world, x_1, x_2 , x_t);
						} 
						if(x_1 % 2 == 0){	
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1 - 1, x_2 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1 - 1 , x_2 , x_t -1);
						} else {
							world->point[current_pos].plaquette_past_neighbour=lat_ctopos(world, x_1 + 1, x_2 ,x_t);
							world->point[current_pos].plaquette_past=lat_ctopos(world, x_1, x_2 , x_t - 1);
						} break;
				}
		
			}
		}
	}
	world->real_time_intersections = calloc(world->size_1 * world->size_2 , sizeof(int));
	world->current_num_rti = 0;
	return 0;	
	}

static void lat_prlatticeslice(lattice * world, int slice)
		{
			printf("\x1b[0m\n"); //Reset the color
			int x_1, x_t ;
			for (x_t=0; x_t < world->size_time; x_t++) {
				for (x_1 = 0; x_1 < world->size_1; x_1++) {
					if (lat_ctopos(world,x_1,slice,x_t), world->point[lat_ctopos(world, x_1, slice, x_t)].is_in_loop)
					{
						printf("\x1B[35m");
					}
					printf("%2d%d ",lat_ctopos(world,x_1,slice,x_t), world->point[lat_ctopos(world, x_1, slice, x_t)].spin);
					printf("\x1B[0m");
					
				}
				printf("\n");
				for (x_1 = 0; x_1 < world->size_1; x_1++) {
					if (world->plaquette[lat_ctopos(world, x_1, slice, x_t)] == 0) printf("    ");
					else if (world->plaquette[lat_ctopos(world, x_1, slice, x_t)] == 1) printf("   H");
					else if (world->plaquette[lat_ctopos(world, x_1, slice, x_t)] == 2) printf("   =");
					else if (world->plaquette[lat_ctopos(world, x_1, slice, x_t)] == 3) printf("   X");

				}
				printf("\n");
				}
			return;
	}
		

//For Debugging
void lat_prlattice(lattice * world)
		{	
				int i;
				for (i = 0; i < world->size_2; i++) {
					lat_prlatticeslice(world, i);
					printf("------\n");
			}
			printf("************\n");
			return;
		}

//initializes two lattices a realtime and a euclidean time lattice
//according to the data in configuration
int lat_linitall(lattice * eucl_world, lattice * real_world, configuration * conf)
{
			
			conf->size_time = conf->size_eucl_time;
			lat_linit(eucl_world, conf);
		
			int i,j;	
			for (i = 0; i < 16; ++i)
			{
				for (j = 0; j < 3; ++j)
				{
					real_world->crules[i][j] = conf->rtcrules[i][j];
					eucl_world->crules[i][j] = conf->etcrules[i][j]; 
				}
					
			}

			
			conf->size_time = conf->size_real_time;
			lat_linit(real_world, conf);
			lat_enforce_cr_consistency(real_world->crules);
			lat_enforce_cr_consistency(eucl_world->crules);

			return 0;
}

//this function is kind of a hack...
//i' really not happy about it.
static int lat_enforce_cr_consistency(double crules[16][3])
{
	int i, j;
	double sum;
	for (i = 0; i < 16; ++i)
	{
		sum = 0;
		for (j = 0; j < 3; ++j)
		{
			sum += crules[i][j]; 				
		}
		if (sum > 0.5)
		{
			if (crules[i][2] > 0)
			{
				crules[i][2] = 1;
			} else if (crules[i][1] > 0)
			{
				crules[i][1] = 1;
			} else 
			{
				crules[i][0] = 1;
			}
		}
	}


	return 0;
}
