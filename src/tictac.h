#ifndef TICTAC
#define TICTAC

#include <time.h>

//starts timer
int tic();

//stops timer
int tac();

//Returns time passed between last tic() and last tac()
float tictac();


#endif
