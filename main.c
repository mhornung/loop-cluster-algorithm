/***************************************************
 * A prototype program to do a simulation...
 * 
 * 
 * With the libraries loop and lattice, loop-cluster monte carlo simulations
 * of varius quantum spin models can be done. In euclidean time as well as 
 * in real time (driven by a measurement process.)
 *
 * The size of the system and various other things can be specisfied as argument to main
 * The Cluster rules are expected to be provided in the specified files.
 *
 * In the File lattice.c the geometry and the input parsing is specified
 * In the File loop.c the loop cluster algorithm is written As a random generator ranlxd is used
 *
 *
 * The following is an example program, wich should 
 * show how to use them.
 *
 ***********************************************/
#include "tictac.h"
#include "loop.h"
#include "lattice.h"
#include "observables.h"
#include "io.h"
#include <math.h>

int welford_update(double * data,double * cvalues, int num_obs, int * n)
{
	double delta;
	int i;
	//update the counter
	(*n)++;
	for (i = 0; i < num_obs; ++i)
	{	
			delta = (double) cvalues[i] - data[2*i];		
			data[2*i] = data[2*i] + delta / (double) (*n);
			data[2*i +1] = data[2 * i + 1] + delta*( (double) cvalues[i] - data[2*i]);
			cvalues[i]= 0;
	}
	return 0;
}

//Normalizes the welford variance to a standard deviation, and then to an uncertainty by dividing trough sqrt(n)
int welford_stddev2error(double * data, int num_obs, int n)
{
	int i;
	for (i = 0; i < num_obs; ++i)
	{
		//Normalize the standard deviation
		data[2*i + 1] = data[2*i + 1]/((double)n - 1 );	
		data[2*i + 1] = sqrt(data[2*i +1]);
		//Convert stdv to an error
		data[2*i + 1] = data[2*i + 1] / sqrt((double) n );
	}	
	return 0;
}


int main(int argc,char *argv[])
{
	tic();
//Various initializations
		lp_initrandgen(); //initializes the random generator with the system time
		/*
		//Two structs are declared. they will contain all information about the 
		//geometry and the current state of the spin system.
		//one represents the real time part, and one the euclidean time part of
		//the simulated system.
		//The pointer to these is passed to all functions wich need information.		
		*/
		lattice real_world;
		lattice eucl_world;
		//The struct conf will contain all information about the external parameters
		//of the system. 
		configuration conf;

		io_getconf(&conf,argc,argv);
		io_printconf(&conf);

		//Memory is allocated to store state and geometry of the system.
		if(lat_linitall(&eucl_world, &real_world, &conf)==1) return 1;

		int i;

		//Compilation time parameters:
		int nsuscmode = (int) ((double) eucl_world.size_1 / 2.);
		int termalizationsteps = 2000;
		int binsize = 100;

		int num_observables = (nsuscmode + 1) * (real_world.size_time);	

		double* results = calloc(2*num_observables, sizeof(double)); //To store each value and each standard deviation
		double* cvalues = calloc(num_observables, sizeof(double)); //To store the values during the simulation and calculate the stddev
		//Thermalize
		for (i = 0; i < termalizationsteps; ++i)
		{
			lp_etloop(&eucl_world);
			lp_rtloop(&real_world, &eucl_world);
			lp_flip(&eucl_world);
			lp_flip(&real_world);

		}
		
		//To calculate standard deviation with welfords algorithm
		//n is incremented by each call of welford_update( ... ) 
		int n = 0;
		int j;

		int sc;
		double suscmode;
		
		//int t;	
		for (i = 0; i < conf.num_steps; i+=1)
		{
			//the state is updated as follows	
			lp_etloop(&eucl_world);
			lp_rtloop(&real_world, &eucl_world);
			lp_flip(&eucl_world);
			lp_flip(&real_world);
		
			for(sc=0; sc <= nsuscmode; sc++)
			{	
				suscmode = 3.141592654 * (double) (sc) / (double) (nsuscmode);

				for (j = 0; j < real_world.size_time; ++j)
				{
					cvalues[sc * real_world.size_time +j] += obs_suscmode(&real_world, j ,suscmode,suscmode);
				}
			}
			if (i %binsize == 0)
			{
				for (j = 0; j < num_observables; ++j)
				{
					cvalues[j]/= (double) binsize;	
				}	
				//io_append_to_file("raw.dat",cvalues,num_observables,real_world.size_time);
				welford_update(results, cvalues, num_observables, &n);
			}	
		}
	
		welford_stddev2error(results, num_observables, n);

		io_append_comment(conf.result_file, conf.comment);
		io_append_to_file(conf.result_file, results, 2*num_observables, 2*(real_world.size_time));
		//Save Observables to file
	printf("%d",n );
	tac();
	printf("\ntime: %lf\n",tictac());
		return 0;
}


