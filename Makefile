TARGET = standalone
LIBS = -lm -Isrc 	
CC = gcc
CFLAGS = -g -Wall -O3

vpath %.c src
vpath %.h src

.PHONY: default all clean 

default: $(TARGET)
all: default

ODIR = bin

OBJECTS = $(addprefix $(ODIR)/, main.o lattice.o  loop.o ranlxs.o observables.o io.o tictac.o) 
HEADERS = lattice.h  loop.h  ranlxs.h observables.h io.h tictac.h

$(ODIR)/%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) $(LIBS) -c $< -o $@ 

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@
	cp $@ test/


clean:
	-rm -f bin/*.o
	-rm -f $(TARGET)


